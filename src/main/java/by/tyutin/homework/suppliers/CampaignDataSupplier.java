package by.tyutin.homework.suppliers;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("/api")
public interface CampaignDataSupplier {
    @GET
    @Path("/campaigns")
    @Consumes(MediaType.APPLICATION_JSON)
    String campaigns();

    @GET
    @Path("/creatives")
    @Consumes(MediaType.APPLICATION_JSON)
    String creatives();
}
