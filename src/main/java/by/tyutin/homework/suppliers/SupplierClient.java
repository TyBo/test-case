package by.tyutin.homework.suppliers;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.UriBuilder;

@Component
public class SupplierClient {
    @Value("${supplier.host}")
    private String host;


    private CampaignDataSupplier campaignDataSupplier;

    @PostConstruct
    public void init() {
        ResteasyClient resteasyClient = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = resteasyClient.target(UriBuilder.fromPath(host));
        campaignDataSupplier = target.proxy(CampaignDataSupplier.class);
    }

    public CampaignDataSupplier getCampaignDataSupplier() {
        return campaignDataSupplier;
    }
}
