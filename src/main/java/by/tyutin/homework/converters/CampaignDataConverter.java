package by.tyutin.homework.converters;

import by.tyutin.homework.dto.Campaign;
import by.tyutin.homework.dto.Creative;
import by.tyutin.homework.dto.answer.CampaignStats;
import by.tyutin.homework.dto.answer.ShortCreativeByCampaign;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class CampaignDataConverter {


    private List<CampaignStats> campaignStats = new ArrayList<>();

    public void convertSupplierData(@NonNull List<Campaign> campaigns, @NonNull List<Creative> creatives) {
        campaignStats.clear();
        campaigns.forEach(campaign -> {
                    CampaignStats campaignStats = new CampaignStats(campaign);
                    creatives.forEach(creative -> {
                        if (Objects.nonNull(creative.getParentId())
                                && creative.getParentId().equals(campaign.getId())) {
                            addToAnswerList(campaignStats, creative);
                        }
                    });
                    this.campaignStats.add(campaignStats);
                }
        );

    }

    private void addToAnswerList(CampaignStats campaignStats, Creative creative) {
        campaignStats.getCreativeList().add(new ShortCreativeByCampaign(creative));
    }

    public List<CampaignStats> getCampaignStats() {
        return campaignStats;
    }
}
