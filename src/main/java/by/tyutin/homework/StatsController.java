package by.tyutin.homework;

import by.tyutin.homework.converters.CampaignDataConverter;
import by.tyutin.homework.dto.Campaign;
import by.tyutin.homework.dto.Creative;
import by.tyutin.homework.parsers.JsonParser;
import by.tyutin.homework.suppliers.SupplierClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ProcessingException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.GZIPOutputStream;

@Log4j2
@RestController
/**
 * @author tyutinbn
 */
public class StatsController {
    @Value("${supplier.host}")
    private String host;

    private String answer;

    private CampaignDataConverter campaignDataConverter;
    private SupplierClient supplierClient;

    public StatsController(@Autowired CampaignDataConverter campaignDataConverter, @Autowired SupplierClient supplierClient) {
        this.campaignDataConverter = campaignDataConverter;
        this.supplierClient = supplierClient;
    }

    @GetMapping(value = "/api/stats")
    @ResponseBody
    public void greeting(HttpServletResponse response) {
        try (OutputStream stream = response.getOutputStream()) {
            try (GZIPOutputStream gzip = new GZIPOutputStream(stream)) {
                collectData();
                convertAnswer();
                response.setHeader("Content-Encoding", "gzip");
                response.setHeader("Vary", "Accept-Encoding");
                response.setContentType("application/json");
                gzip.write(answer.getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            log.error(e);
        }

    }

    public void collectData() {
        if (Strings.isNotBlank(host)) {
            try {
                List<Campaign> campaigns = JsonParser.parseToList(Campaign.class, supplierClient.getCampaignDataSupplier().campaigns());
                List<Creative> creatives = JsonParser.parseToList(Creative.class, supplierClient.getCampaignDataSupplier().creatives());
                campaignDataConverter.convertSupplierData(campaigns, creatives);
            } catch (ProcessingException ex) {
                answer = "cant create client for host, please check your properties and logs";
                log.error(ex);
            } catch (Exception exception) {
                answer = "something wrong with Application, please check logs!";
                log.error(exception);
            }

        } else {
            answer = "property: \"supplier.host\" not installed, please set property and reboot application";
            log.error("property: \"supplier.host\" not installed, please set property and reboot application");
        }
    }

    public void convertAnswer() {
        if (!campaignDataConverter.getCampaignStats().isEmpty()) {
            try {
                answer = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(campaignDataConverter.getCampaignStats());
            } catch (JsonProcessingException exception) {
                log.error(exception);
                answer = "problem with parsing answer, check logs";
            }
        }
    }

    public String getAnswer() {
        return answer;
    }


}
