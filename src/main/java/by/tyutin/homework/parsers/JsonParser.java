package by.tyutin.homework.parsers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class JsonParser {

    public static <T> List<T> parseToList(Class<T> clazz, String json) {
        List<T> answer = new ArrayList<>();
        if (Strings.isNotBlank(json)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                answer = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
            } catch (JsonProcessingException e) {
                log.error(e);
            }
        } else {
            log.error("json is empty");
        }
        return answer;
    }
}
