package by.tyutin.homework.dto;

import java.util.Date;
import java.util.Objects;


public class Campaign {
    private String advertiser;
    private String cpm;
    private Date endDate;
    private Long id;
    private String Name;
    private Date startDate;

    public Campaign() {

    }

    public Campaign(Long id) {
        this.id = id;
    }

    public String getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(String advertiser) {
        this.advertiser = advertiser;
    }

    public String getCpm() {
        return cpm;
    }

    public void setCpm(String cpm) {
        this.cpm = cpm;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Campaign campaign = (Campaign) o;
        return Objects.equals(advertiser, campaign.advertiser) &&
                Objects.equals(cpm, campaign.cpm) &&
                Objects.equals(endDate, campaign.endDate) &&
                Objects.equals(id, campaign.id) &&
                Objects.equals(Name, campaign.Name) &&
                Objects.equals(startDate, campaign.startDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(advertiser, cpm, endDate, id, Name, startDate);
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "advertiser='" + advertiser + '\'' +
                ", cpm='" + cpm + '\'' +
                ", endDate=" + endDate +
                ", id=" + id +
                ", Name='" + Name + '\'' +
                ", startDate=" + startDate +
                '}';
    }
}
