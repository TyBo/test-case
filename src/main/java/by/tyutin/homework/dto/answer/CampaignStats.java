package by.tyutin.homework.dto.answer;

import by.tyutin.homework.dto.Campaign;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class CampaignStats {
    private final String advertiserName;
    private final String campaignName;
    private final Long campaignId;
    private List<ShortCreativeByCampaign> creativeList;

    public CampaignStats(Campaign campaign) {
        this.campaignId = campaign.getId();
        this.advertiserName = campaign.getAdvertiser();
        this.campaignName = campaign.getName();
        this.creativeList = new ArrayList<>();
    }

    public List<ShortCreativeByCampaign> getCreativeList() {
        return creativeList;
    }

    public void setCreativeList(List<ShortCreativeByCampaign> creativeList) {
        this.creativeList = creativeList;
    }

    public String getAdvertiserName() {
        return advertiserName;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignStats that = (CampaignStats) o;
        return Objects.equals(advertiserName, that.advertiserName) &&
                Objects.equals(campaignName, that.campaignName) &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(creativeList, that.creativeList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(advertiserName, campaignName, campaignId, creativeList);
    }

    @Override
    public String toString() {
        return "AnswerResult{" +
                "advertiserName='" + advertiserName + '\'' +
                ", campaignName='" + campaignName + '\'' +
                ", campaignId=" + campaignId +
                ", creativeList=" + creativeList +
                '}';
    }
}
