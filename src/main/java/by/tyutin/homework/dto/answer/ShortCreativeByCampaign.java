package by.tyutin.homework.dto.answer;

import by.tyutin.homework.dto.Creative;

import java.util.Objects;

public class ShortCreativeByCampaign {
    private final Long clicks;
    private final Long id;
    private final Long impression;
    private final Long views;

    public ShortCreativeByCampaign(Creative creative) {
        this.clicks = creative.getClicks();
        this.id = creative.getId();
        this.impression = creative.getImpressions();
        this.views = creative.getViews();
    }

    public Long getClicks() {
        return clicks;
    }

    public Long getId() {
        return id;
    }

    public Long getImpression() {
        return impression;
    }

    public Long getViews() {
        return views;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortCreativeByCampaign that = (ShortCreativeByCampaign) o;
        return Objects.equals(clicks, that.clicks) &&
                Objects.equals(id, that.id) &&
                Objects.equals(impression, that.impression) &&
                Objects.equals(views, that.views);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clicks, id, impression, views);
    }

    @Override
    public String toString() {
        return "ShortCreative{" +
                "clicks=" + clicks +
                ", id=" + id +
                ", impression=" + impression +
                ", views=" + views +
                '}';
    }
}
