package by.tyutin.homework.dto;

import java.util.Objects;


public class Creative {
    private Long clicks;
    private Long conversions;
    private Long id;
    private Long impressions;
    private String name;
    private Long parentId;
    private Long views;

    public Creative() {

    }

    public Creative(Long parentId) {
        this.parentId = parentId;
    }

    public Long getClicks() {
        return clicks;
    }

    public void setClicks(Long clicks) {
        this.clicks = clicks;
    }

    public Long getConversions() {
        return conversions;
    }

    public void setConversions(Long conversions) {
        this.conversions = conversions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getImpressions() {
        return impressions;
    }

    public void setImpressions(Long impressions) {
        this.impressions = impressions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Creative creative = (Creative) o;
        return Objects.equals(clicks, creative.clicks) &&
                Objects.equals(conversions, creative.conversions) &&
                Objects.equals(id, creative.id) &&
                Objects.equals(impressions, creative.impressions) &&
                Objects.equals(name, creative.name) &&
                Objects.equals(parentId, creative.parentId) &&
                Objects.equals(views, creative.views);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clicks, conversions, id, impressions, name, parentId, views);
    }

    @Override
    public String toString() {
        return "Creative{" +
                "clicks=" + clicks +
                ", conversions=" + conversions +
                ", id=" + id +
                ", impressions=" + impressions +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", views=" + views +
                '}';
    }
}
