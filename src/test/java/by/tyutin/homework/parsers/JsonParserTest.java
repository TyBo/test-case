package by.tyutin.homework.parsers;

import by.tyutin.homework.dto.Campaign;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class JsonParserTest {
    String campaigns = "[\n" +
            "  {\n" +
            "    \"advertiser\": \"Telemundo\",\n" +
            "    \"cpm\": \"$12.00\",\n" +
            "    \"endDate\": \"2018-06-28\",\n" +
            "    \"id\": 4976875,\n" +
            "    \"name\": \"cow-pox winos\",\n" +
            "    \"startDate\": \"2018-06-28\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"advertiser\": \"Telemundo\",\n" +
            "    \"cpm\": \"$44.00\",\n" +
            "    \"endDate\": \"2018-06-28\",\n" +
            "    \"id\": 12126023,\n" +
            "    \"name\": \"planity Bearnaise\",\n" +
            "    \"startDate\": \"2018-06-28\"\n" +
            "  }]";

    @Test
    void parseToList() {
        assertThat(JsonParser.parseToList(Campaign.class, campaigns)).isNotEmpty();
        assertThat(JsonParser.parseToList(Campaign.class, null)).isEmpty();
        assertThat(JsonParser.parseToList(Campaign.class, " ")).isEmpty();
    }
}