package by.tyutin.homework.converters;

import by.tyutin.homework.dto.Campaign;
import by.tyutin.homework.dto.Creative;
import by.tyutin.homework.parsers.JsonParser;
import by.tyutin.homework.suppliers.SupplierClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest
class CampaignDataSupplierConverterTest {

    @Autowired
    CampaignDataConverter campaignDataConverter;
    @Autowired
    SupplierClient supplierClient;

    private List<Campaign> campaigns;
    private List<Creative> creatives;

    @BeforeEach
    public void init() {
        campaigns = new ArrayList<>();
        creatives = new ArrayList<>();
        campaigns.add(new Campaign(1L));
        campaigns.add(new Campaign(2L));
        campaigns.add(new Campaign(3L));

        creatives.add(new Creative(1L));
        creatives.add(new Creative(1L));
        creatives.add(new Creative(2L));
    }

    @Test
    void getAnswerResults() {

        campaignDataConverter.convertSupplierData(campaigns, creatives);

        assertThat(campaignDataConverter.getCampaignStats().size() == 3).isTrue();

        creatives.clear();

        campaignDataConverter.convertSupplierData(campaigns, creatives);

        assertThat(campaignDataConverter.getCampaignStats().size() == 3).isTrue();

        campaigns.clear();

        campaignDataConverter.convertSupplierData(campaigns, creatives);

        assertThat(campaignDataConverter.getCampaignStats().size() == 0).isTrue();
    }

    @Test
    public void getRealLifeAnswer() {
        List<Campaign> campaigns = JsonParser.parseToList(Campaign.class, supplierClient.getCampaignDataSupplier().campaigns());
        List<Creative> creatives = JsonParser.parseToList(Creative.class, supplierClient.getCampaignDataSupplier().creatives());
        long start = System.nanoTime();
        campaignDataConverter.convertSupplierData(campaigns, creatives);
        long finish = System.nanoTime();
        long delta = finish - start;
        System.out.println(delta);
        assertThat(campaignDataConverter.getCampaignStats()).isNotEmpty();
    }
}