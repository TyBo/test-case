package by.tyutin.homework;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest
class StatsControllerTest {

    @Autowired
    private StatsController statsController;

    @Test
    public void getAnswer() {
        assertThat(statsController).isNotNull();
        statsController.collectData();
        statsController.convertAnswer();
        assertThat(statsController.getAnswer()).isNotEmpty();
    }

}