package by.tyutin.homework.suppliers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest
class CampaignDataSupplierTest {
    @Autowired
    private SupplierClient supplierclient;


    @Test
    void campaigns() {
        assertThat(supplierclient.getCampaignDataSupplier().campaigns()).isNotEmpty();
    }

    @Test
    void creatives() {
        assertThat(supplierclient.getCampaignDataSupplier().creatives()).isNotEmpty();
    }
}